#!/bin/bash

cd /kafka

printenv

export KAFKAIP

zookeeper="bin/zookeeper-server-start.sh config/zookeeper.properties"
eval "${zookeeper}" &>/dev/null &disown

sed -e "s|\${KAFKAIP}|$KAFKAIP|g" /kafka/config/server.properties.template 
sed -e "s|\${KAFKAIP}|$KAFKAIP|g" /kafka/config/server.properties.template > /kafka/config/server.properties

sleep 5
export JMX_PORT=9997
bin/kafka-server-start.sh config/server.properties
